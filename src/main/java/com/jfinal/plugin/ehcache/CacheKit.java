package com.jfinal.plugin.ehcache;

import io.jboot.Jboot;
import net.sf.ehcache.CacheManager;

public class CacheKit {

    static void init(CacheManager cacheManager) {

    }

    public static void put(String cacheName, Object key, Object value) {
        Jboot.getCache().put(cacheName,key,value);
    }

    public static <T> T get(String cacheName, Object key) {
        return Jboot.getCache().get(cacheName,key);
    }

    public static void remove(String cacheName, Object key) {
        Jboot.getCache().remove(cacheName,key);
    }

    public static <T> T get(String cacheName, Object key, IDataLoader dataLoader) {
        Object data = get(cacheName, key);
        if (data == null) {
            data = dataLoader.load();
            put(cacheName, key, data);
        }
        return (T)data;
    }

    public static void removeAll(String cacheName) {
        Jboot.getCache().removeAll(cacheName);
    }
}
