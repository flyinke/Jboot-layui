/**
 * Copyright 2019-2022 覃海林(qinhaisenlin@163.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.qinhailin.portal.core.ctrl;

import java.sql.SQLException;
import java.util.Date;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Record;
import com.qinhailin.common.base.BaseController;
import com.qinhailin.common.vo.Grid;
import com.qinhailin.common.model.DataSource;
import com.qinhailin.portal.core.service.DataSourceService;

/**
 * 数据源
 * @author qinhailin
 * @date 2022-11-26
 */
@Path("/portal/core/dataSource")
public class DataSourceController extends BaseController {

    @Inject
    DataSourceService service;

  	public void index(){
    	render("index.html");
  	}

    public void list() {
        Record record=getAllParamsToRecord();
        int pageNumber=getParaToInt("pageNumber", 1);
        int pageSize=getParaToInt("pageSize", 10);
        Grid g=service.page(pageNumber, pageSize,record);
        renderJson(g);
	}

    public void add() {
    	render("add.html");
    }

    public void save() {
        DataSource entity=getBean(DataSource.class);
    	entity.setId(createUUID());
    	entity.setCreateTime(new Date());
    	DataSource ds=(DataSource) service.findByPk("config_name", entity.getConfigName());
    	if(ds!=null) {
    		setException("configName已存在，请重新输入");
    	}else { 		
    		entity.save();
    	}
    		
    	setAttr("dataSource", entity);
    	render("add.html");
    }

    public void edit() {
        setAttr("dataSource", service.findById(getPara(0)));
        render("edit.html");
    }

    public void update() {
        DataSource entity=getBean(DataSource.class);
        entity.update();
        setAttr("dataSource", entity);
        render("edit.html");
    }

    public void delete() {
        service.deleteByIds(getIds());
        renderJson(suc());
    }
    
    
    /**
     * 调试数据源链接是否可用
     */
    public void debug() {
    	 DataSource entity=service.getDao().findById(getPara(0));
    	 if(entity==null) {
    		 try {
				service.testConnection(getPara("jdbcUrl"), getPara("user"), getPara("password"));
				renderJson(ok());
			} catch (SQLException e) {
				e.printStackTrace();
				renderJson(fail(e.getMessage()));
			}
    	 } else {
    		 Ret ret=service.testDataSourceConnection(entity);
    		 renderJson(ret);
    	 }
    		 
    }
    
    /**
     * 启动数据源
     */
    public void start() {
    	int start=getParaToInt("start",0);
    	DataSource entity=service.getDao().findById(getPara("id"));
    	if(start==1) {
    		Ret ret=service.startDruidPlugin(entity);
    		renderJson(ret);
    	} else {
    		Ret ret=service.stopDruidPlugin(entity);
    		renderJson(ret);
    	}
    }
    
    /**
	 * 表单修改字段值
	 */
	public void updateFieldValue(){
		boolean b=getModel(DataSource.class).update();
		renderJson(b ? ok() : fail());
	}

}
