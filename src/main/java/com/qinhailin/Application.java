package com.qinhailin;

import cn.hutool.core.convert.Convert;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.server.undertow.WebBuilder;
import com.qinhailin.jboot.ConfigUtil;
import io.jboot.Jboot;
import io.jboot.app.JbootApplication;
import io.jboot.app.JbootWebBuilderConfiger;

public class Application {
    public static void main(String[] args) {
        //此方法错误
//        JbootApplication.setBootArg("config","./config-dev.txt");
        ConfigUtil.readConfigToJboot();
        //标记jboot启动
        JbootApplication.setBootArg("jbootEnable",true);
        JbootWebBuilderConfiger builderConfiger = new JbootWebBuilderConfiger() {
            @Override
            public void onConfig(WebBuilder builder) {

                if(Jboot.configValue("jboot.datasource.factory").equals("druid")){
                    //配置 druid WebStatFilter
                    builder.addFilter("webStatFilter","com.alibaba.druid.support.http.WebStatFilter");
                    builder.addFilterUrlMapping("webStatFilter", "/*");
                    builder.addFilterInitParam("webStatFilter", "exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*,/assets/druid/*,/static/*");

                    builder.addFilterInitParam("webStatFilter", "principalSessionName", "user-session");
                }

                if(Convert.toBool(Jboot.configValue("startUreport2"))) {
                    // 配置 UReport2 Servlet
                    builder.addServlet("ureportServlet", "com.bstek.ureport.console.UReportServlet");
                    builder.addServletMapping("ureportServlet", "/ureport/*");
                    // 配置 Listener
                    builder.addListener("org.springframework.web.context.ContextLoaderListener");
                }

            }
        };
        UndertowServer server = JbootApplication.createServer(args,builderConfiger);
        JbootApplication.start(server);
    }
}
