package com.qinhailin.common.directive;

import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.ParseException;
import com.jfinal.template.stat.Scope;
import com.qinhailin.common.model.DataDictionaryValue;
import com.qinhailin.portal.core.service.DataDictionaryValueService;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

import java.util.List;

/**
 * 下拉Option指令
 */
@JFinalDirective("dataTpl")
public class DataTplDirective extends JbootDirectiveBase {

    @Inject
    private DataDictionaryValueService dataDictionaryValueService;

    /** 数据字典类型编码 */
    private String typeCode;
    /** 属性名默认status，laytpl 里 d.attrName */
    private String attrName;

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        if (exprList.length() > 2) {
            throw new ParseException("Wrong number parameter of #date directive, two parameters allowed at most", location);
        }

        typeCode = getPara(0, scope);
        if (StrKit.isBlank(typeCode)) {
            throw new ParseException("typeCode is null", location);
        }

        if (exprList.length() > 1) {
            attrName = getPara(1, scope,"type");
        }
        List<DataDictionaryValue> list = dataDictionaryValueService.getListByDictionaryCode(typeCode);

        write(writer, "<div>");
        for (DataDictionaryValue data : list) {
            write(writer, "{{#  if(d." + attrName + " == \\'" + data.getValue() + "\\') { }}");
            write(writer, data.getName());
            write(writer, "{{#  } }}");
        }
        write(writer, "</div>");
    }

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

    }
}
