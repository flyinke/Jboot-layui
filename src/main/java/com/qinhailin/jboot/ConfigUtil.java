package com.qinhailin.jboot;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import io.jboot.app.JbootApplication;
import io.jboot.app.config.JbootConfigManager;

import java.util.Properties;

public class ConfigUtil {

    public static void readConfigToJboot(){
        Prop prop = PropKit.use("config-dev.txt");
        prop.getProperties().forEach((k,v) -> {
            if(k == null){
                return;
            }
            JbootApplication.setBootArg(k.toString(),v);
        });
    }

    public static Prop initProp() {
        Properties properties = JbootConfigManager.me().getProperties();
        Prop prop = new JProp().setProperties(properties);
        PropKit.append(prop);
        return prop;
    }
}
