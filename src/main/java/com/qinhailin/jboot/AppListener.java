package com.qinhailin.jboot;

import com.alibaba.fastjson.serializer.SerializeConfig;
import com.jfinal.config.Constants;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.UrlSkipHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.json.FastJsonFactory;
import com.jfinal.json.FastJsonRecordSerializer;
import com.jfinal.kit.Prop;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.template.Engine;
import com.qinhailin.common.config.WebContant;
import com.qinhailin.common.directive.MyNowDirective;
import com.qinhailin.common.handler.CommonHandler;
import com.qinhailin.common.intercepor.ExceptionInterceptor;
import com.qinhailin.common.intercepor.LoggerInterceptor;
import com.qinhailin.common.intercepor.SessionInterceptor;
import com.qinhailin.common.intercepor.TokenInterceptor;
import com.qinhailin.common.kit.DruidKit;
import com.qinhailin.common.kit.IdKit;
import com.qinhailin.common.model._MappingKit;
import com.qinhailin.common.safe.XssHandler;
import io.jboot.aop.jfinal.JfinalHandlers;
import io.jboot.aop.jfinal.JfinalPlugins;
import io.jboot.core.listener.JbootAppListenerBase;
import io.jboot.db.ArpManager;

import java.util.List;

public class AppListener extends JbootAppListenerBase {

    private static Prop p = ConfigUtil.initProp();

    @Override
    public void onConstantConfig(Constants me) {
        // 设置当前是否为开发模式
        me.setDevMode(p.getBoolean("devMode"));
        // 设置默认上传文件保存路径 getFile等使用
        me.setBaseUploadPath(WebContant.baseUploadPath);
        // 设置默认下载文件路径 renderFile使用
        me.setBaseDownloadPath(WebContant.baseDownloadPath);
        // 设置error渲染视图
        me.setError403View(WebContant.error403View);
        me.setError404View(WebContant.error404View);
        me.setError500View(WebContant.error500View);
        //设置 Json 转换工厂实现类
        me.setJsonFactory(FastJsonFactory.me());
        //开启依赖注入
        me.setInjectDependency(true);
        //附件上传大小设置100M
        me.setMaxPostSize(WebContant.maxPostSize);
    }

    @Override
    public void onPluginConfig(JfinalPlugins me) {
        List<ActiveRecordPlugin> activeRecordPlugins = ArpManager.me().getActiveRecordPlugins();
        activeRecordPlugins.forEach(arp -> {
            if(arp.getConfig().getName().equals("main")){
                _MappingKit.mapping(arp);
                //用于模板生成读取模板，这里使用sql格式违反直觉，应该优化
                arp.addSqlTemplate("../"+WebContant.codeTemplate);
            }
        });
    }

    @Override
    public void onInterceptorConfig(Interceptors me) {
        me.addGlobalActionInterceptor(new SessionInViewInterceptor());
        me.addGlobalActionInterceptor(new SessionInterceptor());
        me.addGlobalActionInterceptor(new ExceptionInterceptor());
        me.addGlobalActionInterceptor(new TokenInterceptor());
        me.addGlobalActionInterceptor(new LoggerInterceptor());
    }

    @Override
    public void onHandlerConfig(JfinalHandlers me) {
        /** 配置druid监控 **/
        me.add(DruidKit.getDruidStatViewHandler());
        // 路由处理
        me.add(new CommonHandler());
        // XSS过滤
        me.add(new XssHandler("^\\/portal/form/view.*"));
        // 放开/ureport/开头的请求
        me.add(new UrlSkipHandler("^\\/ureport.*", true));
    }

    @Override
    public void onEngineConfig(Engine me) {

        // 这里只有选择JFinal TPL的时候才用
        me.setDevMode(p.getBoolean("engineDevMode"));
        // 当前时间指令
        me.addDirective("now", MyNowDirective.class);
        // 项目根路径
        me.addSharedObject("path", JFinal.me().getContextPath());
        // 项目名称
        me.addSharedObject("projectName", p.get("projectName"));
        // 项目版权
        me.addSharedObject("copyright", p.get("copyright"));
        // 配置共享函数模板
        me.addSharedFunction(WebContant.functionTemp);
        // 附件在线预览服务
        me.addSharedObject("onlinePreview", p.getBoolean("onlinePreview"));
        me.addSharedObject("onlinePreviewUrl", p.get("onlinePreviewUrl"));
        //允许上传文件类型
        me.addSharedObject("allowUploadFile", WebContant.allowUploadFile);
        //用于前端调用静态方法，创建id
        me.addSharedStaticMethod(IdKit.class);
    }

    @Override
    public void onRouteConfig(Routes me) {

        me.setBaseViewPath(WebContant.baseViewPath);
    }

    @Override
    public void onInit() {

    }

    @Override
    public void onStartFinish() {

    }
}
